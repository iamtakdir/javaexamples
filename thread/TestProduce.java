
public class TestProduce {

  public static void main(String args[]) {
    
    Q q = new Q();
    Produce t1 = new Produce(q);
    Consume t2 = new Consume(q);

    Thread p1 = new Thread(t1);
    Thread c1 = new Thread(t2);

    p1.start();
    c1.start();
  }
  
}


class Q {

  int n;
  boolean valueSet = false;

   synchronized void put(int n) {
    while (valueSet) {
      try {
        wait();
      } catch (InterruptedException e) {
        System.err.println(e);
      }

    }
    this.n = n;
    System.err.printf("Produces %d \n", n);
    valueSet = true;
    notify();
  }

    synchronized void get() {
    
    while (!valueSet) {

      try {
        wait();
      } catch (InterruptedException e) {
        System.err.println(e);
      }

    }
    
    System.err.printf("Consumed %d \n", n);
    valueSet = false;
    notify();

  }
}

class Produce implements Runnable {

  Q q;

  Produce(Q q) {
    this.q = q;
  }

  public void run() {
    
    for (int i = 0; i < 5; i++) {
      q.put(i);
    }
  }


  
}

class Consume implements Runnable {

  Q q;

  Consume(Q q) {
    this.q = q;
  }

  public void run() {
    
    for (int i = 0; i < 5; i++) {
      q.get();
    }
  }

}