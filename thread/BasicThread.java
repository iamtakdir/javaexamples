

public class BasicThread {
  public static void main(String arg[]) {

    int a[] = { 1, 5, 2, 6, 3, 4 };
    String Sa[] = {  "bat",  "dog","Apple","cat", };

    A t1 = new A();
    t1.setA(a);
    t1.start();

    B t2 = new B();
    t2.setA(Sa);
    t2.start();
  }
}

class A extends Thread {

  int a[];

  void setA(int a[]) {
    this.a = a;
  }

  int getNum() {
    return a.length;
  }
  
  public void run() {
    int n = getNum();

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n - i - 1; j++) {
        if (a[j] > a[j + 1]) {
          int temp = a[j];
          a[j] = a[j + 1];
          a[j + 1] = temp;

        }
      }
    }
    // getting array
    for (int i = 0; i < n; i++) {
      System.out.println(a[i]);
    }
   
  }
}

class B extends Thread {
  
  String a[];
 
  void setA(String a[]) {
    this.a = a;

  }

  int getN() {
    return a.length;
  }

  public void run() {
    int n = getN();

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n - i - 1; j++) {
        if (a[j].compareTo(a[j + 1]) > 0) {
          String temp = a[j];
          a[j] = a[j + 1];
          a[j + 1] = temp;
        }
      }
    }

    // getting sorted arr

    for (int i = 0; i < n; i++) {
      System.out.println(a[i]);
    }
  }


}