
class SayHello {

  String name;

  String setName(String name) {
    this.name = name;
    return name;
  }

  public void Hello(String name) {
    setName(name);
    System.err.printf(" Hello %s from SayHello Regular ", name);

  }

  public String getName() {
    return name;
  }
}

class MiniHello extends SayHello implements Runnable {

  public void run() {

    System.err.printf(" Hello %s from Mini Hello " , getName());
    
  }

 
}

public class RunnableThread {

  public static void main(String arg[]) {
    
    MiniHello t1 = new MiniHello();
    t1.Hello("Shishir");

    Thread t = new Thread(t1);

    t.start();

  }
}
