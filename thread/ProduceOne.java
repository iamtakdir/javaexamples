class SharedResource {
  private int value;
  private boolean valueSet = false;

  // Method to produce a value
  public synchronized void produce(int value) {
      // Wait until the previous value has been consumed
      while (valueSet) {
          try {
              wait();
          } catch (InterruptedException e) {
              Thread.currentThread().interrupt();
              System.out.println("Thread interrupted");
          }
      }
      this.value = value;
      valueSet = true;
      System.out.println("Produced: " + value);
      notify(); // Notify the consumer that a new value is available
  }

  // Method to consume a value
  public synchronized void consume() {
      // Wait until a new value is produced
      while (!valueSet) {
          try {
              wait();
          } catch (InterruptedException e) {
              Thread.currentThread().interrupt();
              System.out.println("Thread interrupted");
          }
      }
      valueSet = false;
      System.out.println("Consumed: " + value);
      notify(); // Notify the producer that the value has been consumed
  }
}

class Producer extends Thread {
  private SharedResource resource;

  public Producer(SharedResource resource) {
      this.resource = resource;
  }

  public void run() {
      for (int i = 0; i < 5; i++) {
          resource.produce(i);
          try {
              Thread.sleep(1000); // Sleep for 1 second
          } catch (InterruptedException e) {
              Thread.currentThread().interrupt();
              System.out.println("Producer thread interrupted");
          }
      }
  }
}

class Consumer extends Thread {
  private SharedResource resource;

  public Consumer(SharedResource resource) {
      this.resource = resource;
  }

  public void run() {
      for (int i = 0; i < 5; i++) {
          resource.consume();
          try {
              Thread.sleep(1500); // Sleep for 1.5 seconds
          } catch (InterruptedException e) {
              Thread.currentThread().interrupt();
              System.out.println("Consumer thread interrupted");
          }
      }
  }
}

public class ProduceOne {
  public static void main(String[] args) {
      // Create a shared resource
      SharedResource resource = new SharedResource();

      // Create producer and consumer threads
      Producer producerThread = new Producer(resource);
      Consumer consumerThread = new Consumer(resource);

      // Start the threads
      producerThread.start();
      consumerThread.start();
  }
}
