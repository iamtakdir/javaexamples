

public class InterThread {

public static void main(String[] args) {
  A t1 = new A();
  t1.start();

 /*  try{
    Thread.sleep(100);
  } catch (InterruptedException e) {
    System.out.println(e.getMessage());
  }
 */
  B t2 = new B();
  t2.start();

}
  
}


class A extends Thread {
  
  public void run() {
    
    for (int i = 0; i < 5; i++) {
      System.out.printf(" A -> %d \n", i);
      try{
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        System.out.println("Error in Thread A" + e.getMessage());
      }
    }
  }
}

class B extends Thread {
  
  public void run() {
    
    for (int i = 0; i < 5; i++) {
      System.out.printf(" B -> %d \n", i);
      try{
        Thread.sleep(1050);
      } catch (InterruptedException e) {
        System.out.println("Error in Thread B" + e.getMessage());
      }
    }
  }
}