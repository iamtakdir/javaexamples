
public class InterThreadWithIsAlive {

  public static void main(String args[]) {
    
    A t1 = new A();
    B t2 = new B();

    Thread t11 = new Thread(t1);
    Thread t22 = new Thread(t2);

    t11.start();
    while (t11.isAlive()) {

      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
      
    }
    t22.start();
    
   
  }
  
}

class A implements Runnable{

  public void run() {
    for (int i = 0; i < 5; i++) {
      System.out.printf( "A Finish -> %d \n" ,i );
    }
  }
}

class B implements Runnable{

  public void run() {
    for (int i = 0; i < 5; i++) {
      System.out.printf( "B Start -> %d \n" ,i );
    }
  }
}