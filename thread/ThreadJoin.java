

public class ThreadJoin {

  public static void main(String args[]) throws InterruptedException {
    A t1 = new A();
  B t2 = new B();

  Thread t11 = new Thread(t1);
  Thread t22 = new Thread(t2);

  t11.start();
  t11.join();
t22.start();
  t22.join();
  }
  
  
}


class A implements Runnable {
  public void run() {
    
    for ( int i = 0; i < 5; i++) {
      System.out.printf("A -> %d " ,i);
   }
  }
}

class B implements Runnable {
  public void run() {
    
    for ( int i = 0; i < 5; i++) {
      System.out.printf("B -> %d " ,i);
   }
  }
}
