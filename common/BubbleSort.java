public class BubbleSort {

  public static void main(String arg[]) {
    
    int a[] = { 4, 5, 2, 9, 1, 3 };

    int n = a.length;

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n - i - 1; j++) {

        if (a[j] > a[j + 1]) {
          int temp = a[j];
          a[j] = a[j + 1];
          a[j + 1] = temp;
        }
      }
    }
    
    // printing sorted array 

    for (int i = 0; i < n; i++) {
      System.out.println(a[i]);
    }

  }
  
}
