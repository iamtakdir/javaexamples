public class StringHandle {
  public static void main(String[] args) {
      // 1. equals
      String str1 = "hello";
      String str2 = "Hello";
      
      System.out.println("Using equals:");
      // Case-sensitive comparison
      System.out.println(str1.equals(str2));  // Output: false
      // Case-insensitive comparison
      System.out.println(str1.equalsIgnoreCase(str2));  // Output: true
      System.out.println();

      // 2. compareTo
      String str3 = "apple";
      String str4 = "banana";
      
      System.out.println("Using compareTo:");
      // Comparing str3 and str4
      System.out.println(str3.compareTo(str4));  // Output: -1
      System.out.println();

      // 3. substring
      String str5 = "Hello, world";
      
      System.out.println("Using substring:");
      // Extract substring from index 7 to the end
      String substring1 = str5.substring(7);
      System.out.println(substring1);  // Output: world
      // Extract substring from index 7 to 12 (exclusive)
      String substring2 = str5.substring(7, 12);
      System.out.println(substring2);  // Output: world
      System.out.println();

      // 4. replace
      String str6 = "Hello, world";
      
      System.out.println("Using replace:");
      // Replace all occurrences of 'l' with 'z'
      String replacedStr1 = str6.replace('l', 'z');
      System.out.println(replacedStr1);  // Output: Hezzo, worzd
      // Replace "world" with "Java"
      String replacedStr2 = str6.replace("world", "Java");
      System.out.println(replacedStr2);  // Output: Hello, Java
      System.out.println();

      // 5. StringBuffer
      StringBuffer sb = new StringBuffer("Hello");
      
      System.out.println("Using StringBuffer:");
      // Append " world" to the StringBuffer
      sb.append(" world");
      System.out.println(sb);  // Output: Hello world
      // Reverse the StringBuffer
      sb.reverse();
      System.out.println(sb);  // Output: dlrow olleH
  }
}
