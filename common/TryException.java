import java.util.Scanner;

public class TryException {

  public static void main(String args[]) {

    Scanner ScanNow = new Scanner(System.in);
    System.out.println("Enter number \n");
    int num = ScanNow.nextInt();

    handleArithmeticExp(num);
    handleArrayException(num);

  
  }

  // Handle Arithmetic Exception 

  public static void handleArithmeticExp(int num) {

    try {
      double result = 200 / num;
      System.err.println(result);

    } catch (ArithmeticException e) {
      System.out.println("Arithmetic Error " + e.getMessage());
    }
  }

  // Handle Array Index 

  public static void handleArrayException(int a) {
 
    int[] array = { 1, 2, 3, 4, 5, 6 };

    try{
      System.out.printf("Value %d", array[a]);

    } catch (ArrayIndexOutOfBoundsException e) {
      System.err.println("Error at Array"+ e.getMessage());
    }
  }
  
}

