public class BubbleLexi {

  public static void bubbleSort(String[] arr) {
      int n = arr.length;
      
      // Traverse through all array elements
      for (int i = 0; i < n; i++) {
          // Last i elements are already in place
          for (int j = 0; j < n-i-1; j++) {
              // Swap if the element found is greater than the next element
              if (arr[j].compareTo(arr[j+1]) > 0) {
                  String temp = arr[j];
                  arr[j] = arr[j+1];
                  arr[j+1] = temp;
              }
          }
      }
  }

  public static void printArray(String[] arr) {
      for (String element : arr) {
          System.out.println(element);
      }
  }

  public static void main(String[] args) {
      String[] fruits = {"batman", "bet", "apple"};

      bubbleSort(fruits); // Sort the array using bubble sort
      printArray(fruits); // Print the sorted array
  }
}
