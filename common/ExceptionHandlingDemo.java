import java.io.FileReader;
import java.io.IOException;

// Custom exception class
class CustomException extends Exception {
    public CustomException(String message) {
        super(message);
    }
}

public class ExceptionHandlingDemo {
    public static void main(String[] args) {
        handleArithmeticException();
        handleArrayIndexOutOfBoundsException();
        handleIOException();
        handleNullPointerException();
        handleClassNotFoundException();
        handleCustomException();
    }

    // Handling ArithmeticException
    public static void handleArithmeticException() {
        try {
            int result = 10 / 0; // This will throw ArithmeticException
        } catch (ArithmeticException e) {
            System.out.println("Caught ArithmeticException: " + e.getMessage());
        }
    }

    // Handling ArrayIndexOutOfBoundsException
    public static void handleArrayIndexOutOfBoundsException() {
        try {
            int[] array = new int[5];
            int number = array[10]; // This will throw ArrayIndexOutOfBoundsException
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Caught ArrayIndexOutOfBoundsException: " + e.getMessage());
        }
    }

    // Handling IOException
    public static void handleIOException() {
        try {
            FileReader fr = new FileReader("nonexistentfile.txt"); // This will throw IOException
        } catch (IOException e) {
            System.out.println("Caught IOException: " + e.getMessage());
        }
    }

    // Handling NullPointerException
    public static void handleNullPointerException() {
        try {
            String str = null;
            System.out.println(str.length()); // This will throw NullPointerException
        } catch (NullPointerException e) {
            System.out.println("Caught NullPointerException: " + e.getMessage());
        }
    }

    // Handling ClassNotFoundException
    public static void handleClassNotFoundException() {
        try {
            Class.forName("NonExistentClass"); // This will throw ClassNotFoundException
        } catch (ClassNotFoundException e) {
            System.out.println("Caught ClassNotFoundException: " + e.getMessage());
        }
    }

    // Handling CustomException
    public static void handleCustomException() {
        try {
            checkAge(15); // This will throw CustomException
        } catch (CustomException e) {
            System.out.println("Caught CustomException: " + e.getMessage());
        }
    }

    // Method that throws CustomException
    public static void checkAge(int age) throws CustomException {
        if (age < 18) {
            throw new CustomException("Age is less than 18.");
        } else {
            System.out.println("Age is 18 or older.");
        }
    }
}
