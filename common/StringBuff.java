public class StringBuff {

  public static void main(String arg[]) {
    
    StringBuffer sb = new StringBuffer("Hello");

    // print 

    System.out.println(sb);

    //append 
    sb.append(" World");
    System.out.println(sb);

    //insert 

    sb.insert(0, "Salam ");
    System.out.println(sb);

    //delete

    sb.delete(6, 11);
    System.out.println(sb);

    // reverse
    sb.reverse();
    System.out.println(sb);

  }
  
}

