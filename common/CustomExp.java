
import java.util.Scanner;

public class CustomExp {

  public static void main(String args[]) {

    Scanner ScanNow = new Scanner(System.in);

    int age = ScanNow.nextInt();

     try{
      checkAge(age);
    } catch (CustomErr e) {
      System.out.println("Custom error " + e.getMessage());
     }
    

  }

  public static void checkAge(int age) throws CustomErr {
    
    if (age < 18) {
      throw new CustomErr("Age is not accepted");
    } else {
      System.out.println("Accepted ");
    }
  }

}

class CustomErr extends Exception {

  public CustomErr(String messege) {
    super(messege);
  }
}