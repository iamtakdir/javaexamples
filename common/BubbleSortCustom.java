import java.util.Scanner;

public class BubbleSortCustom {

  public static void main(String arg[]) {

    Scanner ScanNow = new Scanner(System.in);

    System.out.println(" Enter your array size \n");
    int anum = ScanNow.nextInt();
    
    int[] a = new int[anum];

    for (int i = 0; i < anum; i++) {
      System.out.printf(" Enter your number %d = " , i );
      a[i] = ScanNow.nextInt(); 
    }

    int n = a.length;

    BubbleSort(a, n);
  }


  public static void BubbleSort(int a[] , int n ) {
    
    for (int i = 0; i < n; i++) {

      for (int j = 0; j < n - i - 1; j++) {
        if (a[j] > a[j + 1]) {
          int temp = a[j];
          a[j] = a[j + 1];
          a[j + 1] = temp;

        }
      }
    }
    

    // printing arr

    for (int i = 0; i < n; i++){
System.out.println(a[i]);
    }

  }

}


